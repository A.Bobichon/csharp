﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace applcation_console {
    class Program {

        static void Main (string[] args) {
            bool replay = true;

            while (replay == true) {
                string endmessage = game ();
                Console.Write (endmessage);
                Console.WriteLine ("Voulez vous rejouez ? : Y/N");
                string reGame = Console.ReadLine ();
                if (reGame == "N" || reGame == "n") {
                    replay = false;
                }
            }
        }

        static string game () {
            Random randomValue = new Random ();
            int valueGame = randomValue.Next (0, 100);

            // Lance le jeu
            Console.WriteLine ("Bienvenue dans le nombre magique choisi un nombre entre 1 et 100");

            // Le joueur choisi un valeur
            string choixJ = Console.ReadLine ();
            int choixJparse;

            Console.WriteLine (" votre choix est :" + choixJ);

            // si le nombre n'est pas bon
            if (int.TryParse (choixJ, out choixJparse)) {
                choixJparse = Int32.Parse (choixJ);
                while (choixJparse != valueGame) {
                    // si le nombre est trop grand
                    if (choixJparse < valueGame) {
                        Console.WriteLine ("vous etes en dessous");
                        choixJ = Console.ReadLine ();
                        choixJparse = Int32.Parse (choixJ);
                    }
                    // si le nombre est trop petit
                    else if (choixJparse > valueGame) {
                        Console.WriteLine ("vous etes en dessus");
                        choixJ = Console.ReadLine ();
                        choixJparse = Int32.Parse (choixJ);
                    }
                }

                return "Vous avez gagner!!!";
            } else
            {
                return " Choisi des chiffres";
            }
        }
    }

}